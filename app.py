# pip install flask

from distutils.log import debug
from flask import Flask, render_template, request
import json
import torch

from all_classes import TranslationTransformer, beam_search
checkpoint = torch.load('test_config_cpu.json')

model = TranslationTransformer(
    checkpoint['n_tokens_src'],
    checkpoint['n_tokens_tgt'],
    checkpoint['n_heads'],
    checkpoint['dim_embedding'],
    checkpoint['dim_hidden'],
    checkpoint['n_layers'],
    checkpoint['dropout'],
    checkpoint['src_pad_idx'],
    checkpoint['tgt_pad_idx'],
)
model.load_state_dict(checkpoint['model'])

app = Flask(__name__)

@app.route('/')
def acceuil():
    return render_template('acceuil.html') # mettre un href vers l'acceuil ca peut etre sympa 

@app.route('/formulaire', methods=['POST'])
def write_your_sentence():
    if request.method == 'POST':
        sentence = request.form['sentence']
        print(sentence)
        preds = beam_search(
        model,
        sentence,
        checkpoint['src_vocab'],
        checkpoint['tgt_vocab'],
        checkpoint['src_tokenizer'],
        checkpoint['device'],
        beam_width=10,
        max_target=100,
        max_sentence_length=checkpoint['max_sequence_length']
        )[:1]

        for i, (translation, likelihood) in enumerate(preds):
            print(f'{i}. ({likelihood*100:.5f}%) \t {translation}')

    return render_template('acceuil.html', translation=translation) # mettre un href vers l'acceuil ca peut etre sympa 


if __name__ == '__main__':
    app.run(host= '0.0.0.0', port = 5000, debug = True)
    
    