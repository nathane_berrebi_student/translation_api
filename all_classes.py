import numpy as np

import torch
import torch.nn as nn
import einops
from einops.layers.torch import Rearrange
from itertools import takewhile
from torchtext.vocab import Vocab






class RNNCell(nn.Module):
    """A single RNN layer.
    
    Parameters
    ----------
        input_size: Size of each input token.
        hidden_size: Size of each RNN hidden state.
        dropout: Dropout rate.
    """
    def __init__(
            self,
            input_size: int,
            hidden_size: int,
            dropout: float,
        ):
        super().__init__()

        self.W_ih = nn.Linear(input_size, hidden_size)
        self.W_hh = nn.Linear(hidden_size, hidden_size, bias=False)  # We only need one bias

        self.dropout = nn.Dropout(p=dropout)

    def forward(self, x: torch.FloatTensor, h: torch.FloatTensor) -> tuple:
        """Go through all the sequence in x, iteratively updatating
        the hidden state h.

        Args
        ----
            x: Input sequence.
                Shape of [batch_size, seq_len, input_size].
            h: Initial hidden state.
                Shape of [batch_size, hidden_size].

        Output
        ------
            y: Token embeddings.
                Shape of [batch_size, seq_len, hidden_size].
            h: Last hidden state.
                Shape of [batch_size, hidden_size].
        """
        x_ih = self.W_ih(x)  # [batch_size, seq_len, hidden_size]

        y = []
        for token_id in range(x.shape[1]):
            h = torch.tanh(x_ih[:, token_id] + self.W_hh(h))
            y.append(h.unsqueeze(1))

        y = torch.cat(y, dim=1)  # [batch_size, seq_len, hidden_size]
        y = self.dropout(y)
        return y, h

class RNN(nn.Module):
    """Implementation of an RNN based
    on https://pytorch.org/docs/stable/generated/torch.nn.RNN.html.

    Parameters
    ----------
        input_size: Size of each input token.
        hidden_size: Size of each RNN hidden state.
        num_layers: Number of layers (RNNCell or GRUCell).
        dropout: Dropout rate.
        model_type: Either 'RNN' or 'GRU', to select which model we want.
    """
    def __init__(
            self,
            input_size: int,
            hidden_size: int,
            num_layers: int,
            dropout: float,
            model_type: str,
        ):
        super().__init__()
        self.hidden_size = hidden_size
        #cell_cls = RNNCell if model_type == 'RNN' else GRUCell
        cell_cls = RNNCell

        self.cells = nn.ModuleList(
            [cell_cls(input_size, hidden_size, dropout)] + [
                cell_cls(hidden_size, hidden_size, dropout)
                for _ in range(num_layers-1)
            ]
        )

    def forward(self, x: torch.FloatTensor, h: torch.FloatTensor=None) -> tuple:
        """Pass the input sequence through all the RNN cells.
        Returns the output and the final hidden state of each RNN layer

        Args
        ----
            x: Input sequence.
                Shape of [batch_size, seq_len, input_size].
            h: Hidden state for each RNN layer.
                Can be None, in which case an initial hidden state is created.
                Shape of [batch_size, n_layers, hidden_size].

        Output
        ------
            y: Output embeddings for each token after the RNN layers.
                Shape of [batch_size, seq_len, hidden_size].
            h: Final hidden state.
                Shape of [batch_size, n_layers, hidden_size].
        """
        if h is None:
            h = torch.zeros(
                x.shape[0],
                len(self.cells),
                self.hidden_size,
            ).to(x.device)

        final_h = []
        for layer_id in range(len(self.cells)):
            x, h_out = self.cells[layer_id](x, h[:, layer_id])
            final_h.append(h_out.unsqueeze(1))

        return x, torch.cat(final_h, dim=1)

class TranslationRNN(nn.Module):
    """Basic RNN encoder and decoder for a translation task.
    It can run as a vanilla RNN or a GRU-RNN.

    Parameters
    ----------
        n_tokens_src: Number of tokens in the source vocabulary.
        n_tokens_tgt: Number of tokens in the target vocabulary.
        dim_embedding: Dimension size of the word embeddings (for both language).
        dim_hidden: Dimension size of the hidden layers in the RNNs
            (for both the encoder and the decoder).
        n_layers: Number of layers in the RNNs.
        dropout: Dropout rate.
        src_pad_idx: Source padding index value.
        tgt_pad_idx: Target padding index value.
        model_type: Either 'RNN' or 'GRU', to select which model we want.
    """

    def __init__(
            self,
            n_tokens_src: int,
            n_tokens_tgt: int,
            dim_embedding: int,
            dim_hidden: int,
            n_layers: int,
            dropout: float,
            src_pad_idx: int,
            tgt_pad_idx: int,
            model_type: str,
        ):
        super().__init__()

        self.src_embedding = nn.Embedding(
            num_embeddings=n_tokens_src,
            embedding_dim=dim_embedding,
            padding_idx=src_pad_idx,
        )

        self.tgt_embedding = nn.Embedding(
            num_embeddings=n_tokens_tgt,
            embedding_dim=dim_embedding,
            padding_idx=tgt_pad_idx,
        )

        self.encoder = RNN(
            input_size=dim_embedding,
            hidden_size=dim_hidden,
            num_layers=n_layers,
            dropout=dropout,
            model_type=model_type,
        )

        self.norm = nn.LayerNorm(dim_hidden)

        self.decoder = RNN(
            input_size=dim_embedding,
            hidden_size=dim_hidden,
            num_layers=n_layers,
            dropout=dropout,
            model_type=model_type,
        )

        self.head = nn.Sequential(
            nn.Linear(dim_hidden, dim_hidden // 2),
            nn.LeakyReLU(),
            nn.LayerNorm(dim_hidden // 2),

            nn.Linear(dim_hidden // 2, dim_hidden // 2),
            nn.LeakyReLU(),
            nn.LayerNorm(dim_hidden // 2),

            nn.Linear(dim_hidden // 2, dim_hidden // 2),
            nn.LeakyReLU(),
            nn.LayerNorm(dim_hidden // 2),

            nn.Linear(dim_hidden // 2, n_tokens_tgt)
        )

    def forward(
        self,
        source: torch.LongTensor,
        target: torch.LongTensor
    ) -> torch.FloatTensor:
        """Predict the source tokens based on the target tokens.

        Args
        ----
            source: Batch of source sentences.
                Shape of [batch_size, src_seq_len].
            target: Batch of target sentences.
                Shape of [batch_size, tgt_seq_len].
        
        Output
        ------
            y: Batch of predictions where each token is the prediction of
                the next token in the sentence.
                Shape of [batch_size, tgt_seq_len].
        """
        source_emb = self.src_embedding(source)
        target_emb = self.tgt_embedding(target)

        _, hidden = self.encoder(source_emb)
        hidden = self.norm(hidden)
        output, _ = self.decoder(target_emb, hidden)

        output = self.head(output)
        return output


def attention(
        q: torch.FloatTensor,
        k: torch.FloatTensor,
        v: torch.FloatTensor,
        mask: torch.BoolTensor=None,
        dropout: nn.Dropout=None,
    ) -> tuple:
    """Computes multihead scaled dot-product attention from the
    projected queries, keys and values.

    Args
    ----
        q: Batch of queries.
            Shape of [batch_size, seq_len_1, n_heads, dim_model].
        k: Batch of keys.
            Shape of [batch_size, seq_len_2, n_heads, dim_model].
        v: Batch of values.
            Shape of [batch_size, seq_len_2, n_heads, dim_model].
        mask: Prevent tokens to attend to some other tokens (for padding or autoregressive attention).
            Attention is prevented where the mask is `True`.
            Shape of [batch_size, n_heads, seq_len_1, seq_len_2],
            or broadcastable to that shape.
        dropout: Dropout layer to use.

    Output
    ------
        y: Multihead scaled dot-attention between the queries, keys and values.
            Shape of [batch_size, seq_len_1, n_heads, dim_model].
        attn: Computed attention mask.
            Shape of [batch_size, n_heads, seq_len_1, seq_len_2].
    """
    scores = torch.einsum('bihd,bjhd->bhij', (q, k))  # [batch_size, n_heads, seq_len_1, seq_len_2]
    scores = scores / np.sqrt(k.shape[-1])
    if mask is not None:
        scores = scores.masked_fill(mask == True, -1e9)

    attn = torch.softmax(scores, dim=-1)
    if dropout is not None:
        attn = dropout(attn)

    y = torch.einsum('bhij,bjhd->bihd', (attn, v))
    return y, attn


class MultiheadAttention(nn.Module):
    """Multihead attention module.
    Can be used as a self-attention and cross-attention layer.
    The queries, keys and values are projected into multiple heads
    before computing the attention between those tensors.

    Parameters
    ----------
        dim: Dimension of the input tokens.
        n_heads: Number of heads. `dim` must be divisible by `n_heads`.
        dropout: Dropout rate.
    """
    def __init__(
            self,
            dim: int,
            n_heads: int,
            dropout: float,
        ):
        super().__init__()

        assert dim % n_heads == 0

        self.project_queries = nn.Sequential(
            nn.Linear(dim, dim, bias=False),  # Create n_heads queries
            Rearrange('b l (h d) ->  b l h d', h=n_heads),  # Split the queries
        )

        self.project_keys = nn.Sequential(
            nn.Linear(dim, dim, bias=False),
            Rearrange('b l (h d) ->  b l h d', h=n_heads),
        )

        self.project_values = nn.Sequential(
            nn.Linear(dim, dim, bias=False),
            Rearrange('b l (h d) ->  b l h d', h=n_heads),
        )

        self.merge_heads = nn.Sequential(
            Rearrange('b l h d -> b l (h d)'),
            nn.Linear(dim, dim),
        )

        self.dropout = nn.Dropout(dropout)

    def forward(
            self,
            q: torch.FloatTensor,
            k: torch.FloatTensor,
            v: torch.FloatTensor,
            key_padding_mask: torch.BoolTensor = None,
            attn_mask: torch.BoolTensor = None,
        ) -> torch.FloatTensor:
        """Computes the scaled multi-head attention form the input queries,
        keys and values.

        Project those queries, keys and values before feeding them
        to the `attention` function.

        The masks are boolean masks. Tokens are prevented to attends to
        positions where the mask is `True`.

        Args
        ----
            q: Batch of queries.
                Shape of [batch_size, seq_len_1, dim_model].
            k: Batch of keys.
                Shape of [batch_size, seq_len_2, dim_model].
            v: Batch of values.
                Shape of [batch_size, seq_len_2, dim_model].
            key_padding_mask: Prevent attending to padding tokens.
                Shape of [batch_size, seq_len_2].
            attn_mask: Prevent attending to subsequent tokens.
                Shape of [seq_len_1, seq_len_2].

        Output
        ------
            y: Computed multihead attention.
                Shape of [batch_size, seq_len_1, dim_model].
        """
        # Build multihead QKV
        q = self.project_queries(q)  # [batch_size, seq_len_1, n_heads, dim // n_heads]
        k = self.project_keys(k)  # [batch_size, seq_len_1, n_heads, dim // n_heads]
        v = self.project_values(v)  # [batch_size, seq_len_1, n_heads, dim // n_heads]
    
        # Reshape masks and merge them
        if key_padding_mask is None:
            key_padding_mask = torch.zeros((1, 1), dtype=torch.bool).to(q.device)

        if attn_mask is None:
            attn_mask = torch.zeros((1, 1), dtype=torch.bool).to(q.device)

        key_padding_mask = einops.rearrange(
            key_padding_mask, 'b l -> b 1 1 l'
        )  # [batch_size, 1, 1, seq_len_2]
        attn_mask = einops.rearrange(
            attn_mask, 's l -> 1 1 s l'
        )  # [1, 1 seq_len_1, seq_len_2]
        mask = key_padding_mask | attn_mask  # [batch_size, 1, seq_len_1, seq_len_2]

        # Compute Multi-head attention and merge heads
        y, _ = attention(q, k, v, mask, self.dropout)
        y = self.merge_heads(y)
        return y

class TransformerDecoderLayer(nn.Module):
  
  """Single decoder layer.

  Parameters
  ----------
      d_model: The dimension of decoders inputs/outputs.
      dim_feedforward: Hidden dimension of the feedforward networks.
      nheads: Number of heads for each multi-head attention.
      dropout: Dropout rate.
  """

  def __init__(
          self,
          d_model: int,
          d_ff: int,
          nhead: int,
          dropout: float
      ):
      super().__init__()
      self.self_attn = MultiheadAttention(d_model, nhead, dropout)
      self.norm1 = nn.LayerNorm(d_model)
      self.cross_attn = MultiheadAttention(d_model, nhead, dropout)
      self.norm2 = nn.LayerNorm(d_model)
      self.ff = nn.Sequential(
          nn.Linear(d_model, d_ff),
          nn.Dropout(dropout),
          nn.ReLU(),
          nn.Linear(d_ff, d_model)
      )
      self.norm3 = nn.LayerNorm(d_model)

  def forward(
          self,
          src: torch.FloatTensor,
          tgt: torch.FloatTensor,
          tgt_mask_attn: torch.BoolTensor,
          src_key_padding_mask: torch.BoolTensor,
          tgt_key_padding_mask: torch.BoolTensor,
      ) -> torch.FloatTensor:
      """Decode the next target tokens based on the previous tokens.

      Args
      ----
          src: Batch of source sentences.
              Shape of [batch_size, src_seq_len, dim_model].
          tgt: Batch of target sentences.
              Shape of [batch_size, tgt_seq_len, dim_model].
          tgt_mask_attn: Mask to prevent attention to subsequent tokens.
              Shape of [tgt_seq_len, tgt_seq_len].
          src_key_padding_mask: Mask to prevent attention to padding in src sequence.
              Shape of [batch_size, src_seq_len].
          tgt_key_padding_mask: Mask to prevent attention to padding in tgt sequence.
              Shape of [batch_size, tgt_seq_len].

      Output
      ------
          y:  Batch of sequence of embeddings representing the predicted target tokens
              Shape of [batch_size, tgt_seq_len, dim_model].
      """
      # Self-attention
      tgt = self.norm1(
          self.self_attn(tgt, tgt, tgt, tgt_key_padding_mask, attn_mask=tgt_mask_attn) + tgt
      )
      # Cross-attention
      tgt = self.norm2(
          self.cross_attn(tgt, src, src, src_key_padding_mask) + tgt
      )
      # Feedforward
      y = self.norm3(
          self.ff(tgt) + tgt
      )
      return y


class TransformerDecoder(nn.Module):
    """Implementation of the transformer decoder stack.

    Parameters
    ----------
        d_model: The dimension of decoders inputs/outputs.
        dim_feedforward: Hidden dimension of the feedforward networks.
        num_decoder_layers: Number of stacked decoders.
        nheads: Number of heads for each multi-head attention.
        dropout: Dropout rate.
    """

    def __init__(
            self,
            d_model: int,
            d_ff: int,
            num_decoder_layer:int ,
            nhead: int,
            dropout: float
        ):
        super().__init__()
        self.layers = nn.ModuleList([
            TransformerDecoderLayer(d_model, d_ff, nhead, dropout)
            for _ in range(num_decoder_layer)
        ])

    def forward(
            self,
            src: torch.FloatTensor,
            tgt: torch.FloatTensor,
            tgt_mask_attn: torch.BoolTensor,
            src_key_padding_mask: torch.BoolTensor,
            tgt_key_padding_mask: torch.BoolTensor,
        ) -> torch.FloatTensor:
        """Decodes the source sequence by sequentially passing.
        the encoded source sequence and the target sequence through the decoder stack.

        Args
        ----
            src: Batch of encoded source sentences.
                Shape of [batch_size, src_seq_len, dim_model].
            tgt: Batch of taget sentences.
                Shape of [batch_size, tgt_seq_len, dim_model].
            tgt_mask_attn: Mask to prevent attention to subsequent tokens.
                Shape of [tgt_seq_len, tgt_seq_len].
            src_key_padding_mask: Mask to prevent attention to padding in src sequence.
                Shape of [batch_size, src_seq_len].
            tgt_key_padding_mask: Mask to prevent attention to padding in tgt sequence.
                Shape of [batch_size, tgt_seq_len].

        Output
        ------
            y:  Batch of sequence of embeddings representing the predicted target tokens
                Shape of [batch_size, tgt_seq_len, dim_model].
        """
        for decoder_layer in self.layers:
            tgt = decoder_layer(
                src,
                tgt,
                tgt_mask_attn,
                src_key_padding_mask,
                tgt_key_padding_mask,
            )
        return tgt


class TransformerEncoderLayer(nn.Module):
    """Single encoder layer.

    Parameters
    ----------
        d_model: The dimension of input tokens.
        dim_feedforward: Hidden dimension of the feedforward networks.
        nheads: Number of heads for each multi-head attention.
        dropout: Dropout rate.
    """

    def __init__(
            self,
            d_model: int,
            d_ff: int,
            nhead: int,
            dropout: float,
        ):
        super().__init__()

        self.self_attn = MultiheadAttention(d_model, nhead, dropout)

        self.norm1 = nn.LayerNorm(d_model)

        self.feedforward = nn.Sequential(
            nn.Linear(d_model, d_ff),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(d_ff, d_model)
        )

        self.norm2 = nn.LayerNorm(d_model)

    def forward(
        self,
        src: torch.FloatTensor,
        key_padding_mask: torch.BoolTensor
        ) -> torch.FloatTensor:
        """Encodes the input. Does not attend to masked inputs.

        Args
        ----
            src: Batch of embedded source tokens.
                Shape of [batch_size, src_seq_len, dim_model].
            key_padding_mask: Mask preventing attention to padding tokens.
                Shape of [batch_size, src_seq_len].

        Output
        ------
            y: Batch of encoded source tokens.
                Shape of [batch_size, src_seq_len, dim_model].
        """
        src = self.norm1(
            self.self_attn(src, src, src, key_padding_mask) + src
        )
        src = self.norm2(
            self.feedforward(src) + src
        )
        return src


class TransformerEncoder(nn.Module):
    """Implementation of the transformer encoder stack.

    Parameters
    ----------
        d_model: The dimension of encoders inputs.
        dim_feedforward: Hidden dimension of the feedforward networks.
        num_encoder_layers: Number of stacked encoders.
        nheads: Number of heads for each multi-head attention.
        dropout: Dropout rate.
    """

    def __init__(
            self,
            d_model: int,
            dim_feedforward: int,
            num_encoder_layers: int,
            nheads: int,
            dropout: float
        ):
        super().__init__()
        self.layers = nn.ModuleList([
            TransformerEncoderLayer(d_model, dim_feedforward, nheads, dropout)
            for _ in range(num_encoder_layers)
        ])

    def forward(
            self,
            src: torch.FloatTensor,
            key_padding_mask: torch.BoolTensor
        ) -> torch.FloatTensor:
        """Encodes the source sequence by sequentially passing.
        the source sequence through the encoder stack.

        Args
        ----
            src: Batch of embedded source sentences.
                Shape of [batch_size, src_seq_len, dim_model].
            key_padding_mask: Mask preventing attention to padding tokens.
                Shape of [batch_size, src_seq_len].

        Output
        ------
            y: Batch of encoded source sequence.
                Shape of [batch_size, src_seq_len, dim_model].
        """
        for encoder_layer in self.layers:
            src = encoder_layer(src, key_padding_mask)
        return src


class Transformer(nn.Module):
    """Implementation of a Transformer based on the paper: https://arxiv.org/pdf/1706.03762.pdf.

    Parameters
    ----------
        d_model: The dimension of encoders/decoders inputs/ouputs.
        nhead: Number of heads for each multi-head attention.
        num_encoder_layers: Number of stacked encoders.
        num_decoder_layers: Number of stacked encoders.
        dim_feedforward: Hidden dimension of the feedforward networks.
        dropout: Dropout rate.
    """

    def __init__(
            self,
            d_model: int,
            nhead: int,
            num_encoder_layers: int,
            num_decoder_layers: int,
            dim_feedforward: int,
            dropout: float,
        ):
        super().__init__()
        self.encoder = TransformerEncoder(
            d_model,
            dim_feedforward,
            num_encoder_layers,
            nhead,
            dropout
        )

        self.decoder = TransformerDecoder(
            d_model,
            dim_feedforward,
            num_decoder_layers,
            nhead,
            dropout
        )

    def forward(
            self,
            src: torch.FloatTensor,
            tgt: torch.FloatTensor,
            tgt_mask_attn: torch.BoolTensor,
            src_key_padding_mask: torch.BoolTensor,
            tgt_key_padding_mask: torch.BoolTensor
        ) -> torch.FloatTensor:
        """Compute next token embeddings.

        Args
        ----
            src: Batch of source sequences.
                Shape of [batch_size, src_seq_len, dim_model].
            tgt: Batch of target sequences.
                Shape of [batch_size, tgt_seq_len, dim_model].
            tgt_mask_attn: Mask to prevent attention to subsequent tokens.
                Shape of [tgt_seq_len, tgt_seq_len].
            src_key_padding_mask: Mask to prevent attention to padding in src sequence.
                Shape of [batch_size, src_seq_len].
            tgt_key_padding_mask: Mask to prevent attention to padding in tgt sequence.
                Shape of [batch_size, tgt_seq_len].

        Output
        ------
            y: Next token embeddings, given the previous target tokens and the source tokens.
                Shape of [batch_size, tgt_seq_len, dim_model].
        """
        src_embeddings = self.encoder(
            src,
            src_key_padding_mask
        )
        y = self.decoder(
            src_embeddings,
            tgt,
            tgt_mask_attn,
            src_key_padding_mask,
            tgt_key_padding_mask
        )
        return y


class TranslationTransformer(nn.Module):
    """
    Basic Transformer encoder and decoder for a translation task.
    Manage the masks creation, and the token embeddings.
    Position embeddings can be learnt with a standard `nn.Embedding` layer.

    Parameters
    ----------
        n_tokens_src: Number of tokens in the source vocabulary.
        n_tokens_tgt: Number of tokens in the target vocabulary.
        n_heads: Number of heads for each multi-head attention.
        dim_embedding: Dimension size of the word embeddings (for both language).
        dim_hidden: Dimension size of the feedforward layers
            (for both the encoder and the decoder).
        n_layers: Number of layers in the encoder and decoder.
        dropout: Dropout rate.
        src_pad_idx: Source padding index value.
        tgt_pad_idx: Target padding index value.
    """
    def __init__(
            self,
            n_tokens_src: int,
            n_tokens_tgt: int,
            n_heads: int,
            dim_embedding: int,
            dim_hidden: int,
            n_layers: int,
            dropout: float,
            src_pad_idx: int,
            tgt_pad_idx: int,
        ):
        super().__init__()

        self.src_embedding = nn.Embedding(
            num_embeddings=n_tokens_src,
            embedding_dim=dim_embedding,
            padding_idx=src_pad_idx,
        )

        self.tgt_embedding = nn.Embedding(
            num_embeddings=n_tokens_tgt,
            embedding_dim=dim_embedding,
            padding_idx=tgt_pad_idx,
        )

        self.src_pos_embedding = nn.Embedding(1000, dim_embedding)
        self.tgt_pos_embedding = nn.Embedding(1000, dim_embedding)

        self.transformer = Transformer(
            d_model=dim_embedding,
            nhead=n_heads,
            num_encoder_layers=n_layers,
            num_decoder_layers=n_layers,
            dim_feedforward=dim_hidden,
            dropout=dropout,
        )

        self.head = nn.Sequential(
            nn.Linear(dim_embedding, n_tokens_tgt)
        )

        self.src_pad_idx = src_pad_idx
        self.tgt_pad_idx = tgt_pad_idx

    def forward(
            self,
            source: torch.LongTensor,
            target: torch.LongTensor
        ) -> torch.FloatTensor:
        """Predict the target tokens based on the source tokens.

        Args
        ----
            source: Batch of source sentences.
                Shape of [batch_size, seq_len_src].
            target: Batch of target sentences.
                Shape of [batch_size, seq_len_tgt].

        Output
        ------
            y: Batch of predictions of the next token distributions in the target sentences.
                Shape of [batch_size, seq_len_tgt, n_tokens_tgt].
        """
        # Source embeddings and masks
        batch_size, seq_len = source.shape

        src_padding_mask = source == self.src_pad_idx   # [bach_size, n_src_tokens]

        positions = torch.arange(0, seq_len)
        positions = einops.repeat(positions, 's -> b s', b=batch_size).to(source.device)

        source_emb = self.src_embedding(source) + self.src_pos_embedding(positions)

        # Target embeddings and masks
        batch_size, seq_len = target.shape

        tgt_mask = torch.ones((seq_len, seq_len), dtype=torch.bool)
        tgt_mask = torch.triu(tgt_mask, diagonal=1).to(target.device)
        tgt_padding_mask = target == self.tgt_pad_idx

        positions = torch.arange(0, seq_len)
        positions = einops.repeat(positions, 's -> b s', b=batch_size).to(target.device)

        target_emb = self.tgt_embedding(target) + self.tgt_pos_embedding(positions)

        # Transformer forwarding
        output = self.transformer(
            source_emb,
            target_emb,
            tgt_mask_attn=tgt_mask,
            src_key_padding_mask=src_padding_mask,
            tgt_key_padding_mask=tgt_padding_mask,
        )

        # Get token scores
        output = self.head(output)
        return output

def beautify(sentence: str) -> str:
    """Removes useless spaces.
    """
    punc = {'.', ',', ';'}
    for p in punc:
        sentence = sentence.replace(f' {p}', p)
    
    links = {'-', "'"}
    for l in links:
        sentence = sentence.replace(f'{l} ', l)
        sentence = sentence.replace(f' {l}', l)
    
    return sentence

def indices_terminated(
        target: torch.FloatTensor,
        eos_token: int
    ) -> tuple:
    """Split the target sentences between the terminated and the non-terminated
    sentence. Return the indices of those two groups.

    Args
    ----
        target: The sentences.
            Shape of [batch_size, n_tokens].
        eos_token: Value of the End-of-Sentence token.

    Output
    ------
        terminated: Indices of the terminated sentences (who's got the eos_token).
            Shape of [n_terminated, ].
        non-terminated: Indices of the unfinished sentences.
            Shape of [batch_size-n_terminated, ].
    """
    terminated = [i for i, t in enumerate(target) if eos_token in t]
    non_terminated = [i for i, t in enumerate(target) if eos_token not in t]
    return torch.LongTensor(terminated), torch.LongTensor(non_terminated)


def append_beams(
        target: torch.FloatTensor,
        beams: torch.FloatTensor
    ) -> torch.FloatTensor:
    """Add the beam tokens to the current sentences.
    Duplicate the sentences so one token is added per beam per batch.

    Args
    ----
        target: Batch of unfinished sentences.
            Shape of [batch_size, n_tokens].
        beams: Batch of beams for each sentences.
            Shape of [batch_size, n_beams].

    Output
    ------
        target: Batch of sentences with one beam per sentence.
            Shape of [batch_size * n_beams, n_tokens+1].
    """
    batch_size, n_beams = beams.shape
    n_tokens = target.shape[1]

    target = einops.repeat(target, 'b t -> b c t', c=n_beams)  # [batch_size, n_beams, n_tokens]
    beams = beams.unsqueeze(dim=2)  # [batch_size, n_beams, 1]

    target = torch.cat((target, beams), dim=2)  # [batch_size, n_beams, n_tokens+1]
    target = target.view(batch_size*n_beams, n_tokens+1)  # [batch_size * n_beams, n_tokens+1]
    return target


def beam_search(
        model: nn.Module,
        source: str,
        src_vocab: Vocab,
        tgt_vocab: Vocab,
        src_tokenizer,
        device: str,
        beam_width: int,
        max_target: int,
        max_sentence_length: int,
    ) -> list:
    """Do a beam search to produce probable translations.

    Args
    ----
        model: The translation model. Assumes it produces linear score (before softmax).
        source: The sentence to translate.
        src_vocab: The source vocabulary.
        tgt_vocab: The target vocabulary.
        device: Device to which we make the inference.
        beam_width: Number of top-k tokens we keep at each stage.
        max_target: Maximum number of target sentences we keep at the end of each stage.
        max_sentence_length: Maximum number of tokens for the translated sentence.

    Output
    ------
        sentences: List of sentences orderer by their likelihood.
    """
    src_tokens = ['<bos>'] + src_tokenizer(source) + ['<eos>']
    src_tokens = src_vocab(src_tokens)

    tgt_tokens = ['<bos>']
    tgt_tokens = tgt_vocab(tgt_tokens)

    # To tensor and add unitary batch dimension
    src_tokens = torch.LongTensor(src_tokens).to(device)
    tgt_tokens = torch.LongTensor(tgt_tokens).unsqueeze(dim=0).to(device)
    target_probs = torch.FloatTensor([1]).to(device)
    model.to(device)

    EOS_IDX = tgt_vocab['<eos>']
    with torch.no_grad():
        while tgt_tokens.shape[1] < max_sentence_length:
            batch_size, n_tokens = tgt_tokens.shape

            # Get next beams
            src = einops.repeat(src_tokens, 't -> b t', b=tgt_tokens.shape[0])
            predicted = model.forward(src, tgt_tokens)
            predicted = torch.softmax(predicted, dim=-1)
            probs, predicted = predicted[:, -1].topk(k=beam_width, dim=-1)

            # Separe between terminated sentences and the others
            idx_terminated, idx_not_terminated = indices_terminated(tgt_tokens, EOS_IDX)
            idx_terminated, idx_not_terminated = idx_terminated.to(device), idx_not_terminated.to(device)

            tgt_terminated = torch.index_select(tgt_tokens, dim=0, index=idx_terminated)
            tgt_probs_terminated = torch.index_select(target_probs, dim=0, index=idx_terminated)

            filter_t = lambda t: torch.index_select(t, dim=0, index=idx_not_terminated)
            tgt_others = filter_t(tgt_tokens)
            tgt_probs_others = filter_t(target_probs)
            predicted = filter_t(predicted)
            probs = filter_t(probs)

            # Add the top tokens to the previous target sentences
            tgt_others = append_beams(tgt_others, predicted)

            # Add padding to terminated target
            padd = torch.zeros((len(tgt_terminated), 1), dtype=torch.long, device=device)
            tgt_terminated = torch.cat(
                (tgt_terminated, padd),
                dim=1
            )

            # Update each target sentence probabilities
            tgt_probs_others = torch.repeat_interleave(tgt_probs_others, beam_width)
            tgt_probs_others *= probs.flatten()
            tgt_probs_terminated *= 0.999  # Penalize short sequences overtime

            # Group up the terminated and the others
            target_probs = torch.cat(
                (tgt_probs_others, tgt_probs_terminated),
                dim=0
            )
            tgt_tokens = torch.cat(
                (tgt_others, tgt_terminated),
                dim=0
            )

            # Keep only the top `max_target` target sentences
            if target_probs.shape[0] <= max_target:
                continue

            target_probs, indices = target_probs.topk(k=max_target, dim=0)
            tgt_tokens = torch.index_select(tgt_tokens, dim=0, index=indices)

    sentences = []
    for tgt_sentence in tgt_tokens:
        tgt_sentence = list(tgt_sentence)[1:]  # Remove <bos> token
        tgt_sentence = list(takewhile(lambda t: t != EOS_IDX, tgt_sentence))
        tgt_sentence = ' '.join(tgt_vocab.lookup_tokens(tgt_sentence))
        sentences.append(tgt_sentence)

    sentences = [beautify(s) for s in sentences]

    # Join the sentences with their likelihood
    sentences = [(s, p.item()) for s, p in zip(sentences, target_probs)]
    # Sort the sentences by their likelihood
    sentences = [(s, p) for s, p in sorted(sentences, key=lambda k: k[1], reverse=True)]

    return sentences

class TranslationRNN(nn.Module):
    """Basic RNN encoder and decoder for a translation task.
    It can run as a vanilla RNN or a GRU-RNN.

    Parameters
    ----------
        n_tokens_src: Number of tokens in the source vocabulary.
        n_tokens_tgt: Number of tokens in the target vocabulary.
        dim_embedding: Dimension size of the word embeddings (for both language).
        dim_hidden: Dimension size of the hidden layers in the RNNs
            (for both the encoder and the decoder).
        n_layers: Number of layers in the RNNs.
        dropout: Dropout rate.
        src_pad_idx: Source padding index value.
        tgt_pad_idx: Target padding index value.
        model_type: Either 'RNN' or 'GRU', to select which model we want.
    """

    def __init__(
            self,
            n_tokens_src: int,
            n_tokens_tgt: int,
            dim_embedding: int,
            dim_hidden: int,
            n_layers: int,
            dropout: float,
            src_pad_idx: int,
            tgt_pad_idx: int,
            model_type: str,
        ):
        super().__init__()

        self.src_embedding = nn.Embedding(
            num_embeddings=n_tokens_src,
            embedding_dim=dim_embedding,
            padding_idx=src_pad_idx,
        )

        self.tgt_embedding = nn.Embedding(
            num_embeddings=n_tokens_tgt,
            embedding_dim=dim_embedding,
            padding_idx=tgt_pad_idx,
        )

        self.encoder = RNN(
            input_size=dim_embedding,
            hidden_size=dim_hidden,
            num_layers=n_layers,
            dropout=dropout,
            model_type=model_type,
        )

        self.norm = nn.LayerNorm(dim_hidden)

        self.decoder = RNN(
            input_size=dim_embedding,
            hidden_size=dim_hidden,
            num_layers=n_layers,
            dropout=dropout,
            model_type=model_type,
        )

        self.head = nn.Sequential(
            nn.Linear(dim_hidden, dim_hidden // 2),
            nn.LeakyReLU(),
            nn.LayerNorm(dim_hidden // 2),

            nn.Linear(dim_hidden // 2, dim_hidden // 2),
            nn.LeakyReLU(),
            nn.LayerNorm(dim_hidden // 2),

            nn.Linear(dim_hidden // 2, dim_hidden // 2),
            nn.LeakyReLU(),
            nn.LayerNorm(dim_hidden // 2),

            nn.Linear(dim_hidden // 2, n_tokens_tgt)
        )

    def forward(
        self,
        source: torch.LongTensor,
        target: torch.LongTensor
    ) -> torch.FloatTensor:
        """Predict the source tokens based on the target tokens.

        Args
        ----
            source: Batch of source sentences.
                Shape of [batch_size, src_seq_len].
            target: Batch of target sentences.
                Shape of [batch_size, tgt_seq_len].
        
        Output
        ------
            y: Batch of predictions where each token is the prediction of
                the next token in the sentence.
                Shape of [batch_size, tgt_seq_len].
        """
        source_emb = self.src_embedding(source)
        target_emb = self.tgt_embedding(target)

        _, hidden = self.encoder(source_emb)
        hidden = self.norm(hidden)
        output, _ = self.decoder(target_emb, hidden)

        output = self.head(output)
        return output


class RNNCell(nn.Module):
    """A single RNN layer.
    
    Parameters
    ----------
        input_size: Size of each input token.
        hidden_size: Size of each RNN hidden state.
        dropout: Dropout rate.
    """
    def __init__(
            self,
            input_size: int,
            hidden_size: int,
            dropout: float,
        ):
        super().__init__()

        self.W_ih = nn.Linear(input_size, hidden_size)
        self.W_hh = nn.Linear(hidden_size, hidden_size, bias=False)  # We only need one bias

        self.dropout = nn.Dropout(p=dropout)

    def forward(self, x: torch.FloatTensor, h: torch.FloatTensor) -> tuple:
        """Go through all the sequence in x, iteratively updatating
        the hidden state h.

        Args
        ----
            x: Input sequence.
                Shape of [batch_size, seq_len, input_size].
            h: Initial hidden state.
                Shape of [batch_size, hidden_size].

        Output
        ------
            y: Token embeddings.
                Shape of [batch_size, seq_len, hidden_size].
            h: Last hidden state.
                Shape of [batch_size, hidden_size].
        """
        x_ih = self.W_ih(x)  # [batch_size, seq_len, hidden_size]

        y = []
        for token_id in range(x.shape[1]):
            h = torch.tanh(x_ih[:, token_id] + self.W_hh(h))
            y.append(h.unsqueeze(1))

        y = torch.cat(y, dim=1)  # [batch_size, seq_len, hidden_size]
        y = self.dropout(y)
        return y, h

class RNN(nn.Module):
    """Implementation of an RNN based
    on https://pytorch.org/docs/stable/generated/torch.nn.RNN.html.

    Parameters
    ----------
        input_size: Size of each input token.
        hidden_size: Size of each RNN hidden state.
        num_layers: Number of layers (RNNCell or GRUCell).
        dropout: Dropout rate.
        model_type: Either 'RNN' or 'GRU', to select which model we want.
    """
    def __init__(
            self,
            input_size: int,
            hidden_size: int,
            num_layers: int,
            dropout: float,
            model_type: str,
        ):
        super().__init__()
        self.hidden_size = hidden_size
        cell_cls = RNNCell if model_type == 'RNN' else GRUCell

        self.cells = nn.ModuleList(
            [cell_cls(input_size, hidden_size, dropout)] + [
                cell_cls(hidden_size, hidden_size, dropout)
                for _ in range(num_layers-1)
            ]
        )

    def forward(self, x: torch.FloatTensor, h: torch.FloatTensor=None) -> tuple:
        """Pass the input sequence through all the RNN cells.
        Returns the output and the final hidden state of each RNN layer

        Args
        ----
            x: Input sequence.
                Shape of [batch_size, seq_len, input_size].
            h: Hidden state for each RNN layer.
                Can be None, in which case an initial hidden state is created.
                Shape of [batch_size, n_layers, hidden_size].

        Output
        ------
            y: Output embeddings for each token after the RNN layers.
                Shape of [batch_size, seq_len, hidden_size].
            h: Final hidden state.
                Shape of [batch_size, n_layers, hidden_size].
        """
        if h is None:
            h = torch.zeros(
                x.shape[0],
                len(self.cells),
                self.hidden_size,
            ).to(x.device)

        final_h = []
        for layer_id in range(len(self.cells)):
            x, h_out = self.cells[layer_id](x, h[:, layer_id])
            final_h.append(h_out.unsqueeze(1))

        return x, torch.cat(final_h, dim=1)

